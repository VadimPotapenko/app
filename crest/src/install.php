<?php
require_once (__DIR__.'/crest.php');

$result = CRest::installApp();
if($result['rest_only'] === false):?>
<head>
	<style type="text/css">
		h2 {color: #2e2e2e;}
	</style>
	<script src="//api.bitrix24.com/api/v1/"></script>
	<?if($result['install'] == true):?>
	<script>
		BX24.init(function(){
			BX24.callBind('onCrmDealAdd',    'http://dev3.nicedo.ru/vadim/app/app.php');
			BX24.callBind('onCrmDealUpdate', 'http://dev3.nicedo.ru/vadim/app/app.php');
			BX24.installFinish();
		});
	</script>
	<?endif;?>
</head>
<body>
	<?if($result['install'] == true):?>
		<div class="alert alert-primary" role="alert">
			<h2>Установка завершена</h2>
		</div>
	<?else:?>
		<div class="alert alert-danger" role="alert">
			<h2>Ошибка установки</h2>
		</div>
	<?endif;?>
</body>
<?endif;