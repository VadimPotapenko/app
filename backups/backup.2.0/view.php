<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Reporting</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<style type="text/css">
		td:last-child, th:last-child{
			display: none;
		}
	</style>
</head>
<body>

	<!-- фильтр -->
	<div class='d-block p-2 bg-dark text-white'>


		<form action="app.php" class="form-inline" method='post'>

			<div class="d-block text-center">
				<label class="my-1 mr-2" for="inlineFormCustomSelect1">Ответственный  :</label>
				<select class="custom-select my-1 mr-sm-2" id="inlineFormCustomSelect1" name='responsible'>
					<option selected>Выбор ...</option>
					<?php for ($i = 0, $s = count($users); $i < $s; $i++): ?>
						<?php foreach ($users[$i]['result']['result'] as $user): ?>
							<option value="<?=$user['ID']?>"><?=$user['LAST_NAME'].' '.$user['NAME']?></option>
						<?php endforeach; ?>
					<?php endfor; ?>
				</select>
			</div>

			<div class="d-block text-center">
			    <label class="my-1 mr-2" for="inputDate">Дата с :</label>
			    <input type="date" class="form-control my-1 mr-sm-2" name='date_from'>
			</div>

			<div class="d-block text-center">
				<label class="my-1 mr-2" for="inputDate">Дата до :</label>
			    <input type="date" class="form-control my-1 mr-sm-2" name='date_to'>
			</div>

			<div class="col-auto my-1">
				<button type="submit" class="btn btn-primary btn-lg">Отправить</button>
		    </div>

		</form>



	</div>

	<!-- таблица сделок -->
	<table class='table'>
		<thead class="thead-dark">
			<tr>
				<th scope="col">Сделка</th>
				<?php foreach($arrStages as $stage): ?>
					<th scope="col"><?=$stage?></th>
					<th scope="col">Разница</th>
				<?php endforeach; ?>
			</tr>
		</thead>

			<?php for ($i = 0, $size = count($deals); $i < $size; $i++): ?>
				<?php for ($x = 0, $s = count($deals[$i]['result']['result']); $x < $s; $x++): ?>
					<tr>
						<td><?=$deals[$i]['result']['result'][$x]['TITLE']?></td>

						<?php for ($z = 0, $q = count($stagesLoop); $z < $q; $z++): ?>
							<td><?=$deals[$i]['result']['result'][$x]['UF_CRM_'.substr(explode('_', $stagesLoop[$z])[0], 0, 5)] ? 
								$deals[$i]['result']['result'][$x]['UF_CRM_'.substr(explode('_', $stagesLoop[$z])[0], 0, 5)] : '-' ?></td>
							<?php $interval = intervalDays(
								$deals[$i]['result']['result'][$x]['UF_CRM_'.substr(explode('_', $stagesLoop[$z+1])[0], 0, 5)],
								$deals[$i]['result']['result'][$x]['UF_CRM_'.substr(explode('_', $stagesLoop[$z])[0], 0, 5)]
							); ?>
							<td><?=$interval//($interval > 0) ? $interval : 0?></td>
						<?php endfor; ?>
					</tr>

				<?php endfor; ?>
			<?php endfor; ?>

	</table>

	<inpute type='date'>

	<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<!-- <script src="main.js"></script> -->
</body>
</html>