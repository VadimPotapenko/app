<?php
#============================= setting ================================#
include_once (__DIR__.'/crest/src/crest.php');
define ('TYPE_FIELD', 'string');
$filter = array();
$select = array('UF_*', 'TITLE', 'ASSIGNED_BY_ID', 'DATE_CREATE');
#======================================================================#
if ($_REQUEST['DOMAIN'] && !file_exists(__DIR__.'/crest/src/settings.json') && !$_REQUEST['PLACEMENT_OPTIONS']) {
	### установка нового приложения ###
	$install = require (__DIR__.'/crest/src/install.php');
	writeToLog ($result, 'установка приложения завершена');

} elseif ($_REQUEST['event'] == 'ONCRMDEALADD' || $_REQUEST['event'] == 'ONCRMDEALUPDATE') {
	### новое событие ###
	writeToLog($_REQUEST['event'], 'произошло событие');
	$idDeal = $_REQUEST['data']['FIELDS']['ID'];
	$dateUpdate = date('d-m-Y');

	### получаем сделку ###
	$deal = CRest::call('crm.deal.get', array('ID' => $idDeal));
	writeToLog($deal, 'обновленная сделка');

	### готовим имя для пользовательского поля ###
	$fieldName = substr(explode('_', $deal['result']['STAGE_ID'])[0], 0, 5);
	writeToLog($fieldName, 'имя нового пользовательского поля');

	### препятствуем зацикливанию ###
	if (($deal['result']['UF_CRM_'.$fieldName]) && ($deal['result']['UF_CRM_'.$fieldName] != '')) die();

	### поиск пользовательского поля ###
	$bool = false;
	foreach ($deal['result'] as $field => $value) {
		if ($field == 'UF_CRM_'.$fieldName) $bool = true;
	}

	### если поля нет - создаем ###
	if (!$bool) {
		writeToLog('UF_CRM_'.$fieldName, 'создаем новое пользовательское поле');
		$newField = CRest::call('crm.deal.userfield.add', array(
			'fields' => array(
				'FIELD_NAME' => $fieldName,
				'EDIT_FORM_LABEL' => $fieldName,
				'USER_TYPE_ID' => TYPE_FIELD
			)
		));
		writeToLog($newField, 'новое пользовательское поле создано');
	}

	### обновление пользовательского поля ###
	$updateField = CRest::call('crm.deal.update', array(
		'id'     => $idDeal,
		'fields' => array(
			'UF_CRM_'.$fieldName => $dateUpdate
		)
	));
	writeToLog($updateField, 'обновление пользовательского поля');
	
} else {
	### пользователь в приложении ###
	### принимаем post-запрос + формируем фильтр для запроса сделок ###
	if ($_POST['responsible'] && $_POST['date_from'] && $_POST['date_to']) {
		$filter = array('ASSIGNED_BY_ID' => $_POST['responsible'], '>=DATE_CREATE' => $_POST['date_from'], '<DATE_CREATE' => $_POST['date_to']);
		writeToLog($filter, 'запрос от сервера');
	}
	writeToLog($_POST, 'смотрип post-апрос');

	### все пользователи битрикса ###
	$usersTotal = CRest::call ('user.get', array());
	writeToLog($usersTotal, 'всего пользователей в базе');
	if ($usersTotal['total'] > 50) {
		$uIteration = intval($usersTotal['total'] / 50) + 1;
		if ($usersTotal['total'] % 50 == 0) $usersTotal -= 1;
		writeToLog($usersTotal['total'], 'всего пользователей в базе');

		### массив для батч запроса ###
		for ($i = 0; $i < $uIteration; $i++) {
			$start = $i * 50;
			$arrUDeals[] = array(
				'method' => 'user.get',
				'params' => array(
					'start' => $start
				)
			);
		}
		writeToLog($arrUDeals, 'массив для батч-запроса юзеров');

		### вызов батч запроса по юзерам ###
		if (count($arrUDeals) > 50) $arrUDeals = array_chunk($arrUDeals, 50);
		else $arrUDeals = array($arrUDeals);
		for ($i = 0, $s = count($arrUDeals); $i < $s; $i++) {
			$users[] = CRest::callBatch($arrUDeals[$i]);
		}
		writeToLog($user, 'батч-запрос юз');

	} else {
		$users = array(array('result' => $usersTotal));
		writeToLog($users, 'юз без батч запроса');
	}

	### достаем все сделки ###
	$dealsTotal = CRest::call('crm.deal.list', array('filter' => $filter, 'select' => $select));
	if ($dealsTotal['total'] > 50) {
		$iteration = intval($dealsTotal['total'] / 50) + 1;
		if ($dealsTotal['total'] % 50 == 0) $iteration -= 1;
		writeToLog($dealsTotal['total'], 'всего сделок в базе');

		### формируем массив для батч запроса ###
		for ($i = 0; $i < $iteration; $i++) {
			$start = $i * 50;
			$arrDeals[] = array(
				'method' => 'crm.deal.list',
				'params' => array(
					'filter' => $filter,
					'select' => $select,
					'start'  => $start
				)
			);
		}
		writeToLog($arrDeals, 'массив для батч-запроса');

		### вызов батч запроса по сделкам ###
		if (count($arrDeals) > 50) $arrDeals = array_chunk($arrDeals, 50);
		else $arrDeals = array($arrDeals);
		for ($i = 0, $s = count($arrDeals); $i < $s; $i++) {
			$deals[] = CRest::callBatch($arrDeals[$i]);
		}
		writeToLog($deals, 'батч-запрос');

	} else {
		### сделки полученные без батч-запроса, структура подстроенна под батч ###
		$deals = array(array('result' =>$dealsTotal));
		writeToLog($deals, 'сделки без батча');
	}

	### получаем список стадий сделок ###
	$stageId   = CRest::call('crm.dealcategory.stage.list', array());
	writeToLog($stageId, 'стадии сделок из битрикса');

	$stageNot  = array('APOLOGY', 'LOSE', 'WON');
	foreach ($stageId['result'] as $value) {
		if (!in_array($value['STATUS_ID'], $stageNot)) {
			$arrStages[$value['STATUS_ID']] = $value['NAME'];
		}
	}
	writeToLog($arrStages, 'массив со стадиями сделок');

	$stagesLoop = array_keys($arrStages);
	writeToLog($stagesLoop, 'ключи стадий сделок');

	### верстка ###
	include_once(__DIR__.'/view.php');
}

############################ functions ##############################
function writeToLog ($data, $title = 'DEBUG', $file = 'debug.txt') {
	$log = "\n--------------------\n";
	$log .= date('d.m.Y H:i:s')."\n";
	$log .= $title."\n";
	$log .= print_r($data, 1);
	$log .= "\n--------------------\n";
	file_put_contents(__DIR__.'/'.$file, $log, FILE_APPEND);
	return true;
}

function intervalDays($CheckIn, $CheckOut){
	$CheckInX = explode("-", $CheckIn);
	$CheckOutX =  explode("-", $CheckOut);
	$date1 =  mktime(0, 0, 0, $CheckInX[1],$CheckInX[0],$CheckInX[2]);
	$date2 =  mktime(0, 0, 0, $CheckOutX[1],$CheckOutX[0],$CheckOutX[2]);
	if (!$date1) $date1 = $date2;
	$interval =($date1 - $date2) / (3600*24);
	return $interval;
}