<?php
#============================== setting ===================================#
session_start();
include_once (__DIR__.'/crest/src/crest.php');
$select = array('UF_*', 'TITLE', 'ASSIGNED_BY_ID', 'DATE_CREATE');
### настройка имени портала ###
$server = $_REQUEST['DOMAIN'] ?? $_POST['server'] ?? $_REQUEST['auth']['domain'] ?? explode('&', explode('=', $_SERVER['argv'][0])[1])[0];
if (empty($_SESSION['domain'])) $_SESSION['domain'] = $server;
### настройки логирования
$fileName = 'logs/'.$server.'.txt';  // куда логируем
$logs = false;                      // false - отмена логирования
#==========================================================================#
writeToLog($_REQUEST, $fileName, $logs, 'Новый запрос');
if ($_REQUEST['DOMAIN'] && !file_exists(__DIR__.'/crest/src/clients/'.$_SESSION['domain'].'.json')) {
	### установка нового приложения ###
	$install = require (__DIR__.'/crest/src/install.php');
	writeToLog($_REQUEST['DOMAIN'], $fileName, $logs, 'Установка приложения новым пользователем');

} elseif ($_REQUEST['event'] == 'ONCRMDEALADD' || $_REQUEST['event'] == 'ONCRMDEALUPDATE') {
	### новое событие ###
	$idDeal = $_REQUEST['data']['FIELDS']['ID'];
	$dateUpdate = date('d-m-Y');
	### получаем сделку и готовим имя для пользовательского поля ###
	$deal = CRest::call('crm.deal.get', array('ID' => $idDeal));
	$fieldName = substr(explode('_', $deal['result']['STAGE_ID'])[0], 0, 7);
	if (($deal['result']['UF_CRM_'.$fieldName]) && ($deal['result']['UF_CRM_'.$fieldName] != '')) die();  // препятствуем зацикливанию
	writeToLog($deal, $fileName, $logs, 'Обновленная сделка');
	writeToLog($fieldName, $fileName, $logs, 'Создаем новое пользовательское поле');

	### поиск пользовательского поля ###
	$bool = false;
	foreach ($deal['result'] as $field => $value) {
		if ($field == 'UF_CRM_'.$fieldName) $bool = true;
	}
	### если поля нет - создаем ###
	if (!$bool) {
		$newField = CRest::call('crm.deal.userfield.add', array(
			'fields' => array(
				'FIELD_NAME' => $fieldName,
				'EDIT_FORM_LABEL' => $fieldName,
				'USER_TYPE_ID' => 'string'
			)
		));
		writeToLog($updateField, $fileName, $logs, 'Создано пользовательское поле');
	}
	### обновление пользовательского поля ###
	$updateField = CRest::call('crm.deal.update', array(
		'id'     => $idDeal,
		'fields' => array(
			'UF_CRM_'.$fieldName => $dateUpdate
		)
	));
	writeToLog($updateField, $fileName, $logs, 'Обновлено пользовательское поле');
	
} else {
	### настройки фильтра по-дефолту ###
	$dateFilter = date('Y-m-d', time());
	$filter = array('>=DATE_CREATE' => $dateFilter, '<DATE_CREATE' => $dateFilter);
	### принимаем post-запрос + формируем фильтр для запроса сделок ###
	if ($_POST['responsible'] && $_POST['date_from'] && $_POST['date_to']) {
		$filter = array('ASSIGNED_BY_ID' => $_POST['responsible'], '>=DATE_CREATE' => $_POST['date_from'], '<DATE_CREATE' => $_POST['date_to']);
	}

	### все пользователи битрикса ###
	$usersTotal = CRest::call ('user.get', array('filter' => array('ACTIVE' => 'Y')));
	if ($usersTotal['total'] > 50) {
		$uIteration = intval($usersTotal['total'] / 50) + 1;
		if ($usersTotal['total'] % 50 == 0) $usersTotal -= 1;
		### массив для батч запроса ###
		for ($i = 0; $i < $uIteration; $i++) {
			$start = $i * 50;
			$arrUDeals[] = array(
				'method' => 'user.get',
				'params' => array(
					'filter' => array('ACTIVE' => 'Y'),
					'start' => $start
				)
			);
		}
		if (count($arrUDeals) > 50) $arrUDeals = array_chunk($arrUDeals, 50);
		else $arrUDeals = array($arrUDeals);
		for ($i = 0, $s = count($arrUDeals); $i < $s; $i++) {
			$res_users[] = CRest::callBatch($arrUDeals[$i]);
		}

		for ($i = 0, $s = count($res_users); $i < $s; $i++) {
			foreach ($res_users[$i]['result']['result'] as $value) {
				$users[]['result'] = $value;
			}
		}

	} else {
		$users = array($usersTotal);
	}
	writeToLog($users, $fileName, $logs, 'Состояние приложения: пользователи');

	### достаем все сделки ###
	$dealsTotal = CRest::call('crm.deal.list', array('filter' => $filter, 'select' => $select));
	if ($dealsTotal['total'] > 50) {
		$iteration = intval($dealsTotal['total'] / 50) + 1;
		if ($dealsTotal['total'] % 50 == 0) $iteration -= 1;
		### формируем массив для батч запроса ###
		for ($i = 0; $i < $iteration; $i++) {
			$start = $i * 50;
			$arrDeals[] = array(
				'method' => 'crm.deal.list',
				'params' => array(
					'filter' => $filter,
					'select' => $select,
					'start'  => $start
				)
			);
		}
		if (count($arrDeals) > 50) $arrDeals = array_chunk($arrDeals, 50);
		else $arrDeals = array($arrDeals);
		for ($i = 0, $s = count($arrDeals); $i < $s; $i++) {
			$res_deals[] = CRest::callBatch($arrDeals[$i]);
		}

		for ($i = 0, $s = count($res_deals); $i < $s; $i++) {
			foreach ($res_deals[$i]['result']['result'] as $value) {
				$deals[]['result'] = $value;
			}
		}

	} else {
		$deals = array($dealsTotal);
	}
	writeToLog($deals, $fileName, $logs, 'Состояние приложения: сделки');

	### получаем список стадий сделок ###
	$stageId   = CRest::call('crm.dealcategory.stage.list', array());
	$stageNot  = array('APOLOGY', 'LOSE', 'WON');
	foreach ($stageId['result'] as $value) {
		if (!in_array($value['STATUS_ID'], $stageNot)) $arrStages[$value['STATUS_ID']] = $value['NAME'];
	}
	$stagesLoop = array_keys($arrStages);
	writeToLog($stagesLoop, $fileName, $logs, 'Состояние приложения: стадии');
	include_once(__DIR__.'/view.php'); // верстка
}

############################### functions #################################
function writeToLog ($data, $file, $bool, $title = 'DEBUG') {
	if ($bool) {
		$log = "\n--------------------\n";
		$log .= date('d.m.Y H:i:s')."\n";
		$log .= $title."\n";
		$log .= print_r($data, 1);
		$log .= "\n--------------------\n";
		file_put_contents(__DIR__.'/'.$file, $log, FILE_APPEND);
	}
	return true;
}

function intervalDays($CheckIn, $CheckOut){
	$CheckInX = explode("-", $CheckIn);
	$CheckOutX =  explode("-", $CheckOut);
	$date1 =  mktime(0, 0, 0, $CheckInX[1],$CheckInX[0],$CheckInX[2]);
	$date2 =  mktime(0, 0, 0, $CheckOutX[1],$CheckOutX[0],$CheckOutX[2]);
	if (!$date2) $date2 = 0;
	if (!$date1) $date1 = $date2;
	$interval =($date1 - $date2) / (3600*24);
	return $interval;
}